const promptDirectory = require('inquirer-select-directory')

module.exports = function (plop) {
  plop.setPrompt('directory', promptDirectory)
  // controller generator
  plop.setGenerator('project', {
      description: 'ImportGenius Microservice Project Boilerplate',
      prompts: [
        {
          type: 'input',
          name: 'project-name',
          message: 'Project name(sample-service)',
          default: 'sample-service'
        },
        {
          type: 'input',
          name: 'project-version',
          message: 'Version(1.0.0)',
          default: '1.0.0',
          validate: (version) => 
            /^[\d]{1,}\.[\d]{1,}\.[\d]{1,}$/.test(version)
        },
        {
          type: 'input',
          name: 'project-description',
          message: 'Description(optional)'
        },
        {
          type: 'input',
          name: 'project-repository',
          message: 'Repository(optional)'
        },
        {
          type: 'input',
          name: 'project-author',
          message: 'Author\'s name(code-ninja)',
          default: 'code-ninja'
        },
        {
          type: 'directory',
          name: 'project-directory',
          message: 'Where should we create your project directory?',
          basePath: '.'
        }
      ],
      actions: [
        {
          type: 'addMany',
          destination: '{{project-directory}}/{{project-name}}',
          base: 'plop-templates/new-microservice',
          templateFiles: ['plop-templates/new-microservice/**', 'plop-templates/new-microservice/.*'],
          abortOnFail: true
        }
      ]
  })
}
