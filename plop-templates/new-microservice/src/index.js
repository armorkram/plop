import { createServer } from '~/servers/createServer'

const HOST = process.env.HOST || 3000

createServer(() => console.log(`Server is now listening at port ${HOST}`))
