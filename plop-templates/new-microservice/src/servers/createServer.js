import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import errorHandler from '@/middleware/default/errorHandler'
import notFound from '@/middleware/default/notFound'

const HOST = process.env.HOST || 3000

const app = express()
app.use(bodyParser.json())
app.use(cors)

// @TODO READ src/routes

/**
 * 
 * @param {*} done 
 * @param {*} injectableRoutes - array of routers to be passed to app.use
 */
export function createServer(done, injectableRoutes) {
  if (injectableRoutes) {
    injectableRoutes.forEach(router => app.use(router))
  }

  app.use(notFound)
  app.use(errorHandler)

  return {
    server: app.listen(HOST, done)
  };
}
